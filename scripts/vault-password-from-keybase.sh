#!/bin/sh

KEYBASE_PATH=/keybase/team/epfl_nemesis_ops/ansible-vault-password-file.txt

bail_out() {
    cat >&2
    echo most-certainly-not-the-Vault-password
    exit 0
}

which keybase 2>/dev/null || bail_out <<NO_KEYBASE

WARNING: keybase not installed, will be unable to decipher and use Vault secrets

NO_KEYBASE

keybase fs read "$KEYBASE_PATH" || bail_out <<KEYBASE_TROUBLE

WARNING: keybase failed, will be unable to decipher and use Vault secrets

KEYBASE_TROUBLE

exit 0
