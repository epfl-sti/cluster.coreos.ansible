"""Jinja is Greenspun's Tenth Rule of Python."""

import jinja2


class FilterModule(object):
    def filters(self):
        return {
            'zipdicts': self.zipdicts,
            'flatten': self.flatten
        }

    def zipdicts(self, keys, *value_lists):
        """Like zip, except returns a list of dicts (not a list of lists).

        Arguments:
          keys: The keys to use in each returned dic
          *value_lists: The lists to be combined. There should be the same
                        number of them as there are keys.
        """
        return [dict(zip(keys, values))
                for values in zip(*value_lists)]

    def flatten(self, list_of_lists):
        result = []
        for l in list_of_lists:
            if l is not None and not isinstance(l, jinja2.Undefined):
                result = result + l
        return result
