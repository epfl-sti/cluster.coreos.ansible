import re
from ansible.errors import AnsibleError


class FilterModule(object):
    def filters(self):
        return {
            'ipv4_of_host': self.ipv4_of_host,
            'ipv6_of_host': self.ipv6_of_host
        }

    def _numeric_part_of_hostname(self, hostname):
        matched = re.search('(\d+)$', hostname)
        if not matched:
            AnsibleError('Unable to extract numeric suffix in ' + hostname)
        return str(int(matched.group(0)))

    def ipv4_of_host(self, hostname, prefix):
        return prefix + self._numeric_part_of_hostname(hostname)

    def ipv6_of_host(self, hostname, prefix):
        # Same formula, meaning that we *do not* use the available
        # hex digits from a to f when numbering in IPv6.
        return prefix + self._numeric_part_of_hostname(hostname)
