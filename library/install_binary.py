from __future__ import absolute_import, division, print_function

import os
import subprocess
import json

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.urls import fetch_url
from ansible.module_utils.six.moves.urllib.error import HTTPError

try:
    from ansible.errors import AnsibleError
except ImportError:
    AnsibleError = Exception

__metaclass__ = type

ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['preview'],
                    'supported_by': 'community'}

DOCUMENTATION = '''
---
module: install_binary
short_description: Install a binary at a given version
description:
  - Install a binary at a given version from a URL.
author: "Dominique Quatravaux <dominique@quatravaux.org>"
'''

EXAMPLES = r'''
- name: Install mybinary (checks version by grepping --version output)
  install_binary:
    src: "{{ url }}"
    dst: "/usr/local/bin/mybinary"
    version: "1.0.0"

- name: Install mybinary (custom version flag)
  install_binary:
    src: "{{ url }}"
    dst: "/usr/local/bin/mybinary"
    version: "1.0.0"
    version_cmd: "/usr/local/bin/mybinary version"

- name: Install mybinary (ad-hoc version check)
  install_binary:
    src: "{{ url }}"
    dst: "/usr/local/bin/mybinary"
    version_check: "/usr/local/bin/mybinary version | grep v1.0.0"

- name: Install mybinary from Zip or whatever
  install_binary:
    dst: "/usr/local/bin/mybinary"
    version: "1.0.0"
    install_cmd: "docker run --rm -v /where/ever:/mnt some-image/i-found-on-the-internet sh -c 'cp -a /from/the/image/* /mnt'"
'''


class InstallBinary():
    def __init__(self):
        self.module = AnsibleModule(argument_spec=dict(
            src=dict(type='str', required=False),
            dest=dict(type='str', required=True),
            mode=dict(type='int', default=0o755),
            install_cmd=dict(type='str'),
            version=dict(type='str'),
            version_cmd=dict(type='str'),
            version_check=dict(type='str')))

        self.params = self.module.params

    def run(self):
        ansible_reply = self.check_version()
        if 'failed' not in ansible_reply:
            return self.module.exit_json(**ansible_reply)

        install_cmd = self.params['install_cmd']
        if install_cmd is None:
            self.create_binary(self.get_bin_bytes())
        else:
            install = subprocess.Popen(
                install_cmd, shell=True,
                stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            (stdout, stderr) = install.communicate(b'')
            if install.returncode != 0:
                    raise AnsibleError('install_cmd failed (code %d): %s\n%s\n%s\n' %
                                       (install.returncode, install_cmd, stdout, stderr))

        ansible_reply = self.check_version()
        ansible_reply['changed'] = True
        return self.module.exit_json(**ansible_reply)

    def check_version(self):
        ansible_reply = {}
        if self.params['version_check']:
            # User-specified version check
            version_check = subprocess.Popen(
                self.params['version_check'], shell=True,
                stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            ansible_reply['stdout'], ansible_reply['stderr'] = version_check.communicate(b'')
            if version_check.returncode == 0:
                return ansible_reply
        elif self.params['version']:
            # Do the version check ourselves by substring matching stdout
            # and stderr
            version_cmd = self.params['version_cmd']
            if not version_cmd:
                version_cmd = '%s --version' % self.params['dest']
            version = subprocess.Popen(
                version_cmd, shell=True,
                stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            ansible_reply['stdout'], ansible_reply['stderr'] = version.communicate(b'')
            ansible_reply['returncode'] = version.returncode

            expected_version = self.params['version']
            if (
                    (expected_version in ansible_reply['stdout']) or
                    (expected_version in ansible_reply['stderr'])):
                return ansible_reply
        elif os.access(self.params['dest'], os.X_OK):
            # No version check stipulated
            return {}

        ansible_reply['failed'] = True
        return ansible_reply

    def as_ansible_error(self, error, context=None):
        if isinstance(error, HTTPError):
            res = json.load(error)
            txt = res['message']
        elif isinstance(error, IOError) or isinstance(error, OSError):
            txt = error.strerror
        else:
            txt = str(error)

        if context is not None:
            return AnsibleError('%s: %s' % (context, txt))
        else:
            return AnsibleError(txt)

    def get_bin_bytes(self):
        src = self.params['src']
        try:

            if '://' in src:
                resp, info = fetch_url(self.module, src)
                if info['status'] >= 400:
                    raise AnsibleError('%s: HTTP error %d' % (src, info['status']))
                return resp.read()
            else:
                return open(src, 'rb').read()

        except HTTPError as e:
            raise self.as_ansible_error(e, src)
        except IOError as e:
            raise self.as_ansible_error(e, src)

    def create_binary(self, bin_bytes):
        dest = self.params['dest']
        try:
            with open(dest, 'wb') as to:
                to.write(bin_bytes)
            os.chmod(dest, self.params['mode'])
        except IOError as e:
            raise self.as_ansible_error(e, dest)


def main():
    return InstallBinary().run()


def out_of_ansible_bugware():
    if not hasattr(AnsibleModule, '_remote_tmp'):
        AnsibleModule._remote_tmp = '/tmp'
    if not hasattr(AnsibleModule, '_keep_remote_files'):
        AnsibleModule._keep_remote_files = True


if __name__ == '__main__':
    out_of_ansible_bugware()
    main()
