Role Name
=========

sti.sursync - Local `rsync` wrapper to run it as root (because Ansible won't)

Description
===========

Work around https://github.com/ansible/ansible/issues/34990 by using
an rsync wrapper as the *local* rsync command. This script will be
installed as `/opt/sbin/sursync`, purposefully outside of the `PATH`, and
set as the default rsync command for all hosts (see `../../hosts/vars`)

The *remote* side is correctly, albeit surprisingly, taken care of by
the `become` task parameter (see
https://github.com/ansible/ansible/blob/devel/lib/ansible/plugins/action/synchronize.py#L373)

The rationale of this reversal of the `remote` semantics, insofar as
there is one, is that running rsync as `root` on the `delegate_to`
host, while passing SSH credentials around so that `rsync` can
succeed, is hard. In a Nemesis-class cluster, we side-step this issue
by demanding that the Ansible user use an SSH agent.
`/opt/sbin/sursync` will therefore make sure that `$SSH_AUTH_SOCK` is
set, and fail otherwise.
