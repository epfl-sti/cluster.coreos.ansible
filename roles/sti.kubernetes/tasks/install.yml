---
# Install Kubernetes command-line tools, configuration files and
# prepare kubelet.service for systemd (but don't start it yet)
#
# These tasks are not dependent on any prior bootstrap with kubeadm.

- name: "Directories for Kube binaries, scripts and configuration"
  become: true
  file:
    path: "{{ item }}"
    state: directory
    recurse: true
    owner: root
    group: root
    mode: 0755
  with_items:
    - "{{ kubectl_path | dirname }}"
    - "{{ kubeadm_path | dirname }}"
    - "{{ k8s_pki_dir }}"
    - "{{ kube_static_pods_path }}"  # Assumption: one of these also creates /etc/kubernetes
    - "{{ fake_kubelet_script | dirname }}"

- name: "Version check of {{ crictl_path }}"
  raw: "{{ crictl_path }} --version | grep {{ k8s_version_numeric }}"
  changed_when: false
  ignore_errors: yes
  register: crictl_is_uptodate

- name: "Delete obsolete {{ crictl_path }}"
  when: crictl_is_uptodate is not success
  become: yes
  file:
    path: "{{ crictl_path }}"
    state: absent

- name: "Download and unpack {{ crictl_path }}"
  become: yes
  unarchive:
    src: "{{ crictl_archive_url }}"
    remote_src: yes
    dest: "{{ crictl_path | dirname }}"
  when: crictl_is_uptodate is not success

- name: "Install Kube binaries"
  become: yes
  install_binary:
    src:  "{{ item.url }}"
    dest: "{{ item.path }}"
    version: "{{ k8s_release }}"
    version_cmd: "{{ item.path }} version"
  with_items:
    - url: "{{ kubectl_url }}"
      path: "{{ kubectl_path }}"
    - url: "{{ kubeadm_url }}"
      path: "{{ kubeadm_path }}"

- name: "{{ kubectl_config_dir_for_user_core }}"  # i.e. /home/core/.kube
  file:
    path: "{{ kubectl_config_dir_for_user_core }}"
    state: directory
    mode: 0700
    owner: core
    group: core

################################################################
# Kublet configuration
#
# https://kubernetes.io/docs/setup/independent/install-kubeadm/ says:
# kubeadm will not install *or manage kubelet* or kubectl for you
# (emphasis ours), so configuring it is (mostly) up to us, modulo a
# small systemd drop-in provided by kubeadm's DynamicKubeletConfig
# feature.
#
# We can, however, draw inspiration from the kubelet.service snippets
# at
# https://github.com/coreos/coreos-kubernetes/blob/master/Documentation/deploy-master.md
# and
# https://github.com/coreos/coreos-kubernetes/blob/master/Documentation/deploy-workers.md
# (despite these being clearly labeled as unmaintained - It seems that
# all CoreOS-side support of Kubernetes is focusing on Tectonic these
# days) }

- name: kubelet.service
  become: yes
  copy:
    dest: /etc/systemd/system/kubelet.service
    content: |
      [Unit]
      Description=Kubelet in a box (Docker)
      After=docker.service
      Requires=docker.service

      [Service]
      RestartSec=60s
      TimeoutStartSec=300s
      Restart=always
      ExecStartPre=-/usr/bin/docker rm -f %n
      ExecStop=-/usr/bin/docker rm -f %n
      {# kubeadm seems to be of the opinion that we should use a kubelet
       # installed on-disk. Eh, a Google-built Docker image works too,
       # and perhaps will continue to do so in the future. See also
       # the dud kubectl script below, that only does --version
       #}
      ExecStartPre=-/usr/bin/docker pull gcr.io/google_containers/hyperkube-amd64:{{ kubelet_release }}
      ExecStart=/usr/bin/docker run \
                --rm --name %n \
                --net=host --pid=host --privileged \
                -v "/:/rootfs:ro" \
                -v "/sys:/sys:ro" \
                -v "/var/lib/docker:/var/lib/docker:rw" \
                -v "/var/lib/kubelet:/var/lib/kubelet:rw" \
                -v "/var/lib/calico:/var/lib/calico:rw" \
                -v "/etc/kubernetes:/etc/kubernetes:rw" \
                -v "/etc/cni:/etc/cni:ro" \
                -v "/opt/cni/bin:/opt/cni/bin:ro" \
                -v "/var/run:/var/run:rw" \
                -v "/usr/sbin/modprobe:/usr/sbin/modprobe:ro" \
                -v "/lib/modules:/lib/modules:ro" \
                "gcr.io/google_containers/hyperkube-amd64:{{ kubelet_release }}" \
                /hyperkube kubelet --containerized \
                --node-ip {{ my_ipv6 }} \
                --network-plugin=cni \
                --kubeconfig=/etc/kubernetes/kubelet.conf \
                --bootstrap-kubeconfig=/etc/kubernetes/bootstrap-kubelet.conf \
                --pod-manifest-path={{ kube_static_pods_path }} \
                --dynamic-config-dir={{ kubeadm_kubelet_dynamic_config_dir }} \
                {% if is_master -%}
                --node-labels=node-role.kubernetes.io/master= \
                --register-with-taints=node-role.kubernetes.io/master=:NoSchedule \
                {# --register-schedulable=false: Kubelet comes online cordoned.
                 #
                 # This looks like it is redundant with the taint
                 # above, and indeed --register-schedulable is
                 # deprecated in favor of taints in the source code;
                 # however, users are in a position to bypass taints
                 # (by adding tolerations to their pods), unless even
                 # more magic is applied (something called the
                 # PodTolerationRestriction admission controller,
                 # which unlike this flag, requires extra
                 # configuration). So basically we intend to hold onto
                 # this flag for as long as they'll let us. #}
                --register-schedulable=false \
                {% endif -%}
                --hostname-override={{ ansible_fqdn }}

      [Install]
      WantedBy=multi-user.target
  register: _kubelet_systemctl_service_file

- name: Reload changed kubelet.service
  when: _kubelet_systemctl_service_file is changed
  become: yes
  shell: |
    set -e -x
    systemctl daemon-reload
    systemctl enable kubelet
    systemctl try-restart kubelet

################################################################
# Kubeadm configuration file and wrapper script
#
- name: "{{ kubeadm_config_file }}"
  become: true
  # TODO: We should re-bootstrap whenever this file changes -
  # How do we do that without downing the kubelet for a minute or so?
  copy:
    dest: "{{ kubeadm_config_file }}"
    content: |
      # This file is managed by Ansible (sti.kubernetes/tasks/kubeadm.yml)
      # See https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/#config-file
      apiVersion: kubeadm.k8s.io/v1alpha2
      kind: MasterConfiguration
      kubernetesVersion: {{ k8s_release }}
      clusterName: {{ cluster_name }}
      {# etcd is *not* bootstrapped and managed by kubeadm.
       # Upside: we can manage this crucial resource for the benefit of
       # other stakeholders (stackholders?) such as Calico
       # Downside: etcd doesn't get TLS protection. Payloads must be
       # firewalled from ever reaching the IPv4 physical nodes. #}
      networking:
        dnsDomain: {{ cluster_dns_domain }}
        podSubnet: {{ workload_cidrv6 }}
        serviceSubnet: {{ service_cidrv6 }}

      etcd:
         external:
           endpoints:
             - http://127.0.0.1:2379/
      apiServerCertSANs:
        - "{{ cluster_openshift_public_name }}"
      api:
        {# Advertise ourselves at our IPv6 address, as per
         # https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/#initializing-your-master -#}
        advertiseAddress: "{{ my_ipv6 }}"
        {# On the other hand we want IPv4 here for the Kubelet -> apiserver
         # traffic, because [::1] is not mentioned in the apiserver
         # certificate -#}
        controlPlaneEndpoint: "127.0.0.1:{{ kube_apiserver_port }}"
      featureGates:
        {# https://kubernetes.io/docs/tasks/administer-cluster/reconfigure-kubelet/ -#}
        DynamicKubeletConfig: true
        {# SelfHosting
         # (https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/#self-hosting)
         # looks promising, but it doesn't work on bare metal (won't
         # survive a reboot) -#}
        {# StoreCertsInSecrets is also promising, but requires SelfHosting -#}
        HighAvailability: true  {#- But watch out https://github.com/kubernetes/kubeadm/issues/751 #}

- name: "{{ kubeadm_wrapper_script }} wrapper script"
  become: true
  copy:
    mode: 0755
    dest: "{{ kubeadm_wrapper_script }}"
    content: |
      #!/bin/bash
      #
      # kubeadm wrapper script created by Ansible
      #
      # Lets operators and scripts alike forget about the quirks
      # required to run kubeadm in a Nemesis-class cluster

      with_config=

      case "$1" in
          init)
            with_config=t
            ;;
          alpha)
            case "$2" in
              certs)
                : ;;
              *)
                with_config=t ;;
            esac ;;
          join)
            export PATH={{ fake_kubelet_script | dirname }}:$PATH
            ;;
      esac

      exec {{ kubeadm_path }} \
          ${with_config:+ --config "{{ kubeadm_config_file }}"} \
          "$@"

- name: "{{ fake_kubelet_script }}"
  become: true
  copy:
    mode: 0755
    owner: root
    group: root
    dest: "{{ fake_kubelet_script }}"
    content: |
      #!/bin/bash
      #
      # Fake Kubelet script used to fool kubeadm's version detection logic
      # (because --ignore-preflight-errors=KubeletVersion only gets you
      # so far as of v1.11)
      #
      # Created by Ansible, do not edit

      case "$1" in
        --version) echo "Kubernetes {{ k8s_release }}" ;;
        *) exit 1 ;;
      esac
