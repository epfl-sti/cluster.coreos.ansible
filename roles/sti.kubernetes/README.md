# sti.kubernetes - Manage a kubernetes cluster sti style

## Summary

This role joins a node into the Kubernetes cluster, either as a worker
node or as a master node.

It takes care of the following operations (in sequence):

1. Install or upgrade required binaries and wrapper scripts under `/opt/k8s/bin` and `/opt/bin`
1. Create static (credential-free) configuration files
1. Handle *almost* all the node bootstrapping tasks with Kubeadm

## STI-specific Kubernetes flavor

- Node classification: every node is either
  - a **worker node** dedicated to user workloads,
  - or a **master node** that runs the Kube API server and various other
    daemons of the [Kubernetes control
    plane](https://kubernetes.io/docs/concepts/#kubernetes-control-plane)
    and nothing else (no user workloads)
- [API server worker loadbalancer](tasks/apiserver-worker-lb.yml):
  every worker node runs a loadbalancer (based on haproxy but that
  doesn't really matter), so that it is possible to reach the API
  server at https://127.0.0.1:6443 on all nodes. (On master nodes, the
  local Kubernetes apiserver listens on that port; on worker nodes,
  the haproxy comes into play)
- Networking
  - All containers (except the --net=host ones) run IPv6
  - [Calico](https://www.projectcalico.org/) is used to provision IPv6
    addresses and provide routing and network isolation
- etcd: Kubernetes uses the **insecure** [IPv4-only etcd
  quorum](../sti.etcd) as its data store, relying on the tenants
  lacking access to the physical IPv4 address space for security.

## Bootstrapping and Crentials

The main thing that needs cryptographic credentials is the Kubelet, so
that it can take orders from the API server and run things. The
Kubelet authenticates with a private key / certificate pair embedded
within a YAML configuration file, where the certificate is dynamically
signed by the API server during the first handshake (pretty much like
Puppet does); see more details below.

Compared to a worker, a Kubernetes master is just a Kubelet with
additional pods, e.g. API server, controller manager and scheduler
(together called the [Kubernetes control
plane](https://kubernetes.io/docs/concepts/#kubernetes-control-plane)).
Bootstrapping consists of providing these pods with additional
credentials (more YAML configuration files with certificates and
keys), and configuring Kubelet restrictions in etcd (like "don't run
ordinary tasks here").

In a Nemesis-class cluster, all bootstrapping is performed with
[`kubeadm`](https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/).
Here is what happens when bootstrapping the Kubelet credentials
(regardless of worker or master status):

1. Pre-condition: one API server must be already up and running — Out
   of fear of automation-aidede mishaps, nemsible won't do this for
   you (peruse the Kubeadm documentation to find out how to get
   that done manually - ["single-master"
   flow](https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/)
   is fine; unlike etcd it is straightforward to add more API servers
   regardless of prior planning, or lack thereof)
2. On the existing API server we say `kubeadm token create` and take
   note of the single-use token that it outputs (actually, we remember
   the whole `kubeadm init` command line that gets printed to stdout)
3. On the new node we run the aforementioned `kubeadm init` command.
   Kubeadm contacts the API server and leverages the token to create
   `/etc/kubernetes/bootstrap-kubelet.conf`.
4. Now we (Ansible) can run the new Kubelet and it takes things from
   there: reads credentials from
   `/etc/kubernetes/bootstrap-kubelet.conf`, generates a private key,
   sends an X509 Certificate Signing Request (CSR) to send to the API
   server, and waits for it to come back signed as a certificate.
   (Depending on cluster configuration, the operator may have to type
   `kubectl get csr` and `kubectl certificate approve` while the new
   Kubelet is waiting.) When that is all done, the newly-minted member
   Kubelet creates its own `/etc/kubernetes/kubelet.conf` (with the
   private key and certificate inside).

How kubeadm upgrades a running Kubelet to master status:

- [kubeadm
  v1.11](https://v1-11.docs.kubernetes.io/docs/setup/independent/high-availability/#add-the-third-stacked-control-plane-node):
  after kubeadm join like you would a worker, say<pre>kubeadm alpha phase kubeconfig all
 kubeadm alpha phase controlplane all
 kubeadm alpha phase mark-master
</pre>
- [kubeadm v1.13](https://v1-13.docs.kubernetes.io/docs/setup/independent/high-availability/#add-the-third-stacked-control-plane-node): there is a `--experimental-control-plane` flag to pass to `kubeadm join`

In both cases, additional config files (with credentials) will be
created for the control plane pods to volume-mount and use, e.g.
`/etc/kubernetes/controller-manager.conf`

### <a name="bootstrap-files"></a> Table of Credential-Bearing Files

| Files | Description     | Created by            | Used by   |
| ---- | ---------------- | ----------------------| ----------- |
| `/etc/kubernetes/pki/ca.key`<br/>`/etc/kubernetes/pki/ca.crt`<br/> (master nodes) | The assets of the Certification Authority (CA) that signs all the other k8s certificates | Initial (manual) run of `kubeadm init`, then synced by Ansible among master nodes  | `kube-controller-manager`, through its `--cluster-signing-key-file` flag |
| `/etc/kubernetes/admin.conf`<br/> (master nodes) | The credentials to be wielded by the operator using the `kubectl` command | `kubeadm init` (first master node)<br/> `kubeadm alpha phase kubeconfig all` (other master nodes) | Not used in this path; see `/home/core/.kube/config` below |
| `/etc/kubernetes/pki/ca.crt` (all Kubelets) | The Kubernetes PKI's CA certificate | `kubeadm init`, then synced by `kubeadm join` onto all nodes | <ul><li> `kube-controller-manager`, through its `--cluster-signing-cert-file` flag</li><li> ?? Other? (The CA certificate is also Base64-encoded in a number of `.conf` files, so it is unclear whether worker Kubelets use this file at all)</li></ul> |
| `/etc/kubelet/bootstrap-kubelet.conf` | Temporary Kubelet configuration file, containing an authentication token with 1-hour TTL | `kubeadm join` | Kubelet when beginning the TLS bootstrap process |
| `/etc/kubelet/kubelet.conf` | Main Kubelet configuration file, with API server address, client credentials and server certificate / key<br/> 💡 If this file exists on a worker, then this Kubelet has been healthy at least once. | <ul><li>Masters: `kubeadm alpha phase certs all`</li><li>Workers: Kubelet itself, during the TLS bootstrap process</li></ul> | Kubelet, in steady state |
| `/etc/kubelet/controller-manager.conf`<br/>`/etc/kubelet/scheduler.conf`<br/>(masters) | Credentials wielded by the corresponding control plane daemons when talking to the API server | `kubedam join` (v1.13)<br/>`kubeadm alpha phase controlplane all` (v1.11) | `kube-api-server`<br/>`kube-scheduler` |
| `/home/core/.kube/config` | API server address and credentials for the `kubectl` command, when run by user `core` | Ansible (copied from `/etc/kubernetes/admin.conf` on any master node) | `kubectl` command |


## External Variables

This role depends on the following variables being defined:

`k8s_version`
:   The version to install for kubectl and the Kubernetes system
    workloads: apiserver, controller-manager etc. (from
    quay.io/coreos/hyperkube)

`kubelet_version`
:   The Kubelet version to install (from
    gcr.io/google_containers/hyperkube-amd64, *not* the CoreOS-provided
    rkt image that is lagging behind in terms of versions at the moment,
    and doesn't work with "pure" Calico).

## Playbooks

An unusual subdirectory in this role is `playbooks`. It contains
`unkube.yml` that can be used to de-configure Kubernetes on a node.
Using it first is the recommended and supported way to downgrade a
node from master to worker (Ansible will *not* do that on its own).
See usage instructions in at the top of `playbooks/unkube.yml`
