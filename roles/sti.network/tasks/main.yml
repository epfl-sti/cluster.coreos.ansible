###########################################################################
# networkd-style networking configuration
#
# This is the "main" part, to execute after Python has been installed.
# See crude.yml for the bare-bones amount of work that sti.bootstrap
# will invoke, just so that it can download something.
#
# On every node, gateway or not, the main IPv4 and IPv6 address are on
# a device named ethbr4, which as the name implies is a bridge. Doing
# this (rather than renaming the underlying interface i.e. bond0 for
# an internal node or e.g. enp1s0f0 for a gateway node; see below)
# lets us perform shrewd networking tricks if required (i.e. we can
# bridge L2 payloads such as a TFTP server into ethbr4).
#
# Gateway nodes (plural - Highly available!) use only one of their
# physical Ethernet ports for this bridge, while the other ones use
# bonding. For now, we use a conservative "active-passive" policy. We
# don't have (or want) switch-side support for LACP bonding, but we
# could experiment with one of these fancy new load-balancing modes
# such as "balance-tlb" and "balance-alb"

- setup:

- name: "/etc/hosts"
  become: true
  copy:
    dest: "/etc/hosts"
    content: |
      # /etc/hosts is managed with Ansible, DO NOT EDIT

      ::1             localhost
      127.0.0.1       localhost kubernetes

      {% for item in groups[cluster_name + '-all'] | sort %}
      {{ item | ipv6_of_host(ipv6_physical_prefix) }}	{{ item }}.{{cluster_dns_domain}}	{{ item }}
      {{ item | ipv4_of_host(ipv4_internal_prefix) }}	{{ item }}.{{cluster_dns_domain}}	{{ item }}
      {% endfor %}

############ Configuration of the physical interfaces ##################

- name: "Internal interface(s)"
  loop: "{{ networkd_interfaces }}"
  when: (not is_gateway) or (item.name != gateway_interface)
  become: yes
  copy:
    dest: "{{ item.filename }}"
    owner: root
    group: root
    content: |
      # Network configuration of {{ item.name }}
      #
      # Managed by Ansible, DO NOT EDIT
      #
      [Match]
      Name={{ item.name }}

      [Network]
      DHCP=no
      {% if is_gateway %}
      Bridge=ethbr4
      {% else %}
      Bond=bond0
      {% endif %}
  notify: "restart systemd-networkd"

- name: "Gateway interface"
  when: is_gateway
  become: yes
  copy:
    dest: "{{ gateway_networkd_config_file }}"
    owner: root
    group: root
    content: |
      # Gateway node IPv4 configuration
      #
      # This file is managed by Ansible, DO NOT EDIT

      [Match]
      Name={{ gateway_interface }}

      [Network]
      Address={{ gateway_my_cidrv4 }}
      Gateway={{ gateway_ipv4_default_route }}
      {{ networkd_dns_servers }}
  notify: "restart systemd-networkd"

####################### ethbr4 configuration #########################
# The internal IPv4 and IPv6 addresses are always configured on this bridge.

- name: /etc/systemd/network/ethbr4.netdev
  become: yes
  copy:
    dest: /etc/systemd/network/ethbr4.netdev
    owner: root
    group: root
    content: |
      # Managed by Ansible, DO NOT EDIT
      #
      [NetDev]
      Name=ethbr4
      Kind=bridge
      {% if is_gateway %}
      MACAddress={{ gateway_mac_address }}
      {% endif %}
  notify: "restart systemd-networkd"

- name: /etc/systemd/network/50-ethbr4.network
  become: yes
  copy:
    dest: /etc/systemd/network/50-ethbr4.network
    owner: root
    group: root
    content: |
      # IPv4 / IPv6 configuration of ethbr4
      #
      # Managed by Ansible, DO NOT EDIT
      #
      [Match]
      Name=ethbr4

      [Network]
      Address={{ my_cidrv4 }}
      Address={{ my_cidrv6 }}
      {% if is_gateway %}
      IPMasquerade=yes
      {% else %}
      Gateway={{ my_gateway_vip }}
      {% endif %}
      {{ networkd_dns_servers }}
  notify: "restart systemd-networkd"

################# bond0 configuration (internal nodes only) #################

- name: /etc/systemd/network/bond0.netdev
  when: not is_gateway
  become: yes
  copy:
    dest: /etc/systemd/network/bond0.netdev
    owner: root
    group: root
    content: |
      # Bonding between both physical interfaces of an internal node
      #
      # Managed by Ansible, DO NOT EDIT
      #
      [NetDev]
      Name=bond0
      Kind=bond

      [Bond]
      Mode=active-backup
  notify: "restart systemd-networkd"

- name: /etc/systemd/network/00-bond0.network
  when: not is_gateway
  become: yes
  copy:
    dest: /etc/systemd/network/00-bond0.network
    owner: root
    group: root
    content: |
      # IP configuration of the bonding in an internal node
      # Managed by Ansible, DO NOT EDIT
      #
      [Match]
      Name=bond0

      [Link]
      MACAddress={{ first_mac_address }}

      [Network]
      DHCP=no
      Bridge=ethbr4
  notify: "restart systemd-networkd"

- name: Delete bond0 on gateway node
  when: is_gateway
  loop:
    - /etc/systemd/network/bond0.netdev
    - /etc/systemd/network/00-bond0.network
  become: yes
  file:
    state: absent
    path: "{{ item }}"
  notify: "restart systemd-networkd"

########################### Misc. ######################################

- name: Symlink /etc/resolv.conf
  become: yes
  file:
    src: "../run/systemd/resolve/resolv.conf"
    dest: "/etc/resolv.conf"
    state: link
  register: resolvconf_symlink
  ignore_errors: true

- name: Forcibly symlink /etc/resolv.conf
  become: yes
  raw: "rm -f /etc/resolv.conf && ln -s ../run/systemd/resolve/resolv.conf /etc/resolv.conf"
  when: resolvconf_symlink is failed

- name: /etc/systemd/network/99-fallback-physical.network
  become: yes
  copy:
    dest: /etc/systemd/network/99-fallback-physical.network
    owner: root
    group: root
    content: |
      # Fallback configuration for unmanaged physical interfaces.
      #
      # This file is managed by Ansible, DO NOT EDIT
      #
      #
      # Unless specified otherwise in a 00-foo.network file, physical
      # interfaces are left unconfigured, and do not attempt to DHCP.
      [Match]
      Name=enp*

      [Network]
      DHCP=no
  notify: "restart systemd-networkd"

- name: Clean up old files (Puppet or previous Ansible schemes)
  loop:
    - /etc/systemd/network/50-{{ gateway_interface }}-epflnet.network
    - /etc/systemd/network/50-{{ gateway_interface }}-outside.network
    - /etc/systemd/network/50-ethbr4-internal.network
    - /etc/systemd/network/40-ethbr4-nogateway.network
    - /etc/systemd/network/bond0.network
  become: yes
  file:
    state: absent
    path: "{{ item }}"
  notify: "restart systemd-networkd"

- name: restart systemd-networkd now (if needed)
  meta: flush_handlers

- name: ensure systemd-networkd is running
  become: yes
  service:
    name: systemd-networkd
    state: started
    enabled: yes

####################################################################

- name: "Virtual IPs (VIPs)"
  include_tasks: vip.yml
  when:
    - is_gateway
    - keepalived_failover_shared_secret is defined
