---
# vars file for sti.network

physical_interfaces: "{{ ansible_interfaces | sort | select('match', '^enp') |list }}"
mac_addresses: "{{ physical_interfaces | map('regex_replace', '^(.*)$', 'ansible_\\1') | map('extract', hostvars[inventory_hostname], 'macaddress') | list }}"
networkd_config_files: "{{ physical_interfaces | map('regex_replace', '^(.*)$', '/etc/systemd/network/00-\\1.network') | list }}"

networkd_interfaces: "{{ ['name', 'mac', 'filename'] | zipdicts(physical_interfaces, mac_addresses, networkd_config_files) }}"

first_mac_address: "{{ mac_addresses | first }}"

# The first internal interface being the one that PXE's at boot, we always
# wire up the second one to the external world.
gateway_networkd_config_file: "{{ networkd_config_files | last }}"
first_internal_interface: "{{ physical_interfaces | first }}"
gateway_interface: "{{ physical_interfaces | last }}"
gateway_mac_address: "{{ mac_addresses | last }}"

gateway_my_external_ipv4: "{{ gateway_ipv4_fixed_ips[inventory_hostname] }}"
gateway_my_cidrv4: "{{ gateway_my_external_ipv4 }}/{{ gateway_ipv4_netmask }}"

# All nodes (also gateways) use the same DNS setup.
# For now we just use the EPFL ones, but we will want to improve
# on that at some point.
networkd_dns_servers: |
  {% for dns in external_dns_servers %}
  DNS={{ dns }}
  {% endfor %}

# Round-robin load balanced routing through either of the gateway VIPs
# Thanks to ucarp(8), when a gateway node fails, the VIP stays up
# (i.e. the surviving gateway node picks it up automatically after a
# heartbeat failure)
my_gateway_vip: "{{ ipv4_internal_gateway_vips | map(attribute='ip') | shuffle(seed=inventory_hostname) | first }}"

# There are also ucarp(8)'d VIPs for external-facing traffic
# For ../tasks/vip.yml
vip_short_name: "{{ item.get('name', '') or item['ip'] }}"
gateway_vip_service_name: "{{ vip_short_name | regex_replace('^(.*)$', 'vip-gateway-\\1') }}"
external_vip_service_name: "{{ vip_short_name | regex_replace('^(.*)$', 'vip-external-\\1') }}"
